package jsonprogram;

public class EmpDetails {
		private String name;
		private int age;
		private String city;
		private String company;
		private int id;
		
		public String getName() {
		return name;
		}
		public void setName(String name) {
		this.name = name;
		}
		public int getAge() {
		return age;
		}
		public void setAge(int age) {
		this.age = age;
		}
		public String getCity() {
		return city;
		}
		public void setCity(String city) {
		this.city = city;
		}
		public String getCompany() {
		return company;
			}
		public void setCompany(String company) {
		this.company = company;
			}
		public int getId() {
		return id;
		}
		public void setId(int id) {
		this.id = id ;
			}
		@Override
		public String toString() {
		return "EmpDetails [name=" + name + ", age=" + age + ", city=" + city + ", company =" + company + " , id =" + id +"]";
		}
		}

		

